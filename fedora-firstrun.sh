#!/bin/sh

function base {
  #add the rpmfusion repo for non-free codecs and freetype-freeworld
  echo "=== ADDING RPM FUSION REPO ==="
  dnf install http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm \
              http://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm

  #install codecs and font stuff
  echo "=== INSTALLING CODECS AND FONT MANIPULATION SOFTWARE ==="
  dnf install gstreamer1-libav              \
              gstreamer1-plugins-good       \
              gstreamer1-plugins-bad-free   \
              gstreamer1-plugins-ugly       \
              gstreamer-ffmpeg              \
              gstreamer-plugins-good        \
              gstreamer-plugins-bad         \
              gstreamer-plugins-ugly        \
              gstreamer-plugins-bad-free    \
              gstreamer-plugins-bad-nonfree \
              freetype-freeworld
}

function fonts {
  #fix fonts to look more like Ubuntu:
  echo "=== FIXING FONTS ==="
  gsettings "set" "org.gnome.settings-daemon.plugins.xsettings" "hinting" "slight"
  gsettings "set" "org.gnome.settings-daemon.plugins.xsettings" "antialiasing" "rgba"
  echo "Xft.lcdfilter: lcddefault" >> ~/.Xresources
}

#call the above functions
base
sudo -u $USER fonts
echo "Please logout and log back in or reboot for font changes to take effect"
