**Fedora first run**

This shell script is used to install essential packages for Ubuntu-like font 
rendering and non-free media playback on Fedora Linux. It adds the `rpmfusion`
repo, installs codecs and font configuration utitlites, then executes commands
to change the look of the fonts.

**Instructions**

- Clone the git repo:

    `git clone https://gitlab.com/bitco/fedora-firstrun.git .`
    
- Review the code to make sure it's not malicious!

- Mark the script as executable:

    `chmod +x fedora-firstrun.sh`
    
- Run the executable with sudo:

    `sudo fedora-firstrun.sh`

It may take a few minutes to install everything. Log out and log back in and you should see a difference in font rendering.

What the fonts look like:

![image](http://i.imgur.com/yLe7j7i.png)